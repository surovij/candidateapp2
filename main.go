package main

import (
	"candidateApp2/candidates"
	"candidateApp2/common"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	"go.uber.org/dig"
)

func init() {
	container := initContainer()
	err := container.Invoke(func(s candidates.CandidateService) {
		candidates.CandidateServiceField = s
	})
	if err != nil {
		panic(err)
	}
}

func main() {
	defer func() {
		if r := recover(); r != nil {
			log.Error("Recovered in f", r)
		}
	}()

	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"http://localhost:5000", "https://localhost:5001", "http://localhost:8080"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	}))
	registerRootPath(e)
	candidates.RegisterCandidateRoutes(e)
	e.Logger.Fatal(e.Start(":8080"))
}

func registerRootPath(e *echo.Echo) {
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "")
	})
}

func initContainer() *dig.Container {
	container := dig.New()
	container.Provide(candidates.NewCandidateRepository)
	container.Provide(candidates.NewCandidateService)
	container.Provide(common.NewMongoDbFactoryImpl)
	return container
}
