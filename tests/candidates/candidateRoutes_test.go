package candidates

import (
	. "candidateApp2/candidates"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/ahmetb/go-linq/v3"
	"github.com/golang/mock/gomock"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
)

func setupTest(t *testing.T) *MockCandidateService {

	mockCtrl := gomock.NewController(t)
	var mockCandidateService = NewMockCandidateService(mockCtrl)
	CandidateServiceField = mockCandidateService
	return mockCandidateService

}

func TestRegisterGetAllRoute(t *testing.T) {
	e := echo.New()

	RegisterCandidateRoutes(e)

	getAllRoute := linq.
		From(e.Routes()).
		Where(func(i interface{}) bool {
			return i.(*echo.Route).Path == "/candidates" && i.(*echo.Route).Method == http.MethodGet
		}).
		Single().(*echo.Route)

	assert.Equal(t, http.MethodGet, getAllRoute.Method)
	assert.Equal(t, "/candidates", getAllRoute.Path)
	assert.Equal(t, "candidateApp2/candidates.GetAll", getAllRoute.Name)
}

func TestRegisterGetByIdRoute(t *testing.T) {
	e := echo.New()
	RegisterCandidateRoutes(e)
	route := linq.
		From(e.Routes()).
		Where(func(i interface{}) bool {
			return i.(*echo.Route).Path == "/candidates/:id" && i.(*echo.Route).Method == http.MethodGet
		}).
		Single().(*echo.Route)

	assert.Equal(t, http.MethodGet, route.Method)
	assert.Equal(t, "/candidates/:id", route.Path)
	assert.Equal(t, "candidateApp2/candidates.GetById", route.Name)
}

func TestDeleteRoute(t *testing.T) {
	e := echo.New()
	RegisterCandidateRoutes(e)
	route := linq.
		From(e.Routes()).
		Where(func(i interface{}) bool {
			return i.(*echo.Route).Path == "/candidates/:id" && i.(*echo.Route).Method == http.MethodDelete
		}).
		Single().(*echo.Route)

	assert.Equal(t, http.MethodDelete, route.Method)
	assert.Equal(t, "/candidates/:id", route.Path)
	assert.Equal(t, "candidateApp2/candidates.Delete", route.Name)
}
func TestRegisterCreateRoute(t *testing.T) {
	e := echo.New()

	RegisterCandidateRoutes(e)

	getAllRoute := linq.
		From(e.Routes()).
		Where(func(i interface{}) bool {
			return i.(*echo.Route).Path == "/candidates" && i.(*echo.Route).Method == http.MethodPost
		}).
		Single().(*echo.Route)

	assert.Equal(t, http.MethodPost, getAllRoute.Method)
	assert.Equal(t, "/candidates", getAllRoute.Path)
	assert.Equal(t, "candidateApp2/candidates.Create", getAllRoute.Name)
}

func TestRegisterUpdateRoute(t *testing.T) {
	e := echo.New()

	RegisterCandidateRoutes(e)

	getAllRoute := linq.
		From(e.Routes()).
		Where(func(i interface{}) bool {
			return i.(*echo.Route).Path == "/candidates/:id" && i.(*echo.Route).Method == http.MethodPut
		}).
		Single().(*echo.Route)

	assert.Equal(t, http.MethodPut, getAllRoute.Method)
	assert.Equal(t, "/candidates/:id", getAllRoute.Path)
	assert.Equal(t, "candidateApp2/candidates.Update", getAllRoute.Name)
}

func TestGetAllHandler(t *testing.T) {

	candidatesResponse := []CandidateResponse{{FirstName: "John"}, {FirstName: "Jane"}}
	mockCandidateService := setupTest(t)
	mockCandidateService.
		EXPECT().
		GetCandidates().
		Return(CandidateListResponse{Candidates: candidatesResponse}).
		Times(1)
	defer mockCandidateService.ctrl.Finish()

	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/candidates")
	c.SetHandler(GetAll)
	h := c.Handler()

	if assert.NoError(t, h(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		body, _ := ioutil.ReadAll(rec.Result().Body)
		var m CandidateListResponse
		_ = json.Unmarshal(body, &m)
		assert.Equal(t, 2, len(m.Candidates))
	}
}

func TestGetByIdHandler(t *testing.T) {

	setupTest(t)
	candidateID := "id"
	candidate := CandidateResponse{Id: candidateID}
	mockCandidateService := setupTest(t)
	mockCandidateService.
		EXPECT().
		GetCandidateById(candidateID).
		Return(&candidate, nil).
		Times(1)

	defer mockCandidateService.ctrl.Finish()

	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetParamNames("id")
	c.SetParamValues(candidateID)
	c.SetPath("/candidates/:id")
	c.SetHandler(GetById)
	h := c.Handler()

	if assert.NoError(t, h(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		body, _ := ioutil.ReadAll(rec.Result().Body)
		var m CandidateResponse
		_ = json.Unmarshal(body, &m)
		assert.Equal(t, candidateID, m.Id)
	}
}
func TestGetByIdNotFoundError(t *testing.T) {

	mockCandidateService := setupTest(t)
	candidateID := "id"
	notFoundError := &CandidateNotFoundError{Id: candidateID}
	mockCandidateService.
		EXPECT().
		GetCandidateById(candidateID).
		Return(nil, notFoundError).
		Times(1)
	defer mockCandidateService.ctrl.Finish()

	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetParamNames("id")
	c.SetParamValues(candidateID)
	c.SetPath("/candidates/:id")
	c.SetHandler(GetById)
	h := c.Handler()
	resp := h(c)
	if assert.Error(t, resp) {
		err := resp.(*echo.HTTPError)
		assert.Equal(t, http.StatusNotFound, err.Code)

	}
}

func TestCreateCandidateHandler(t *testing.T) {

	setupTest(t)
	candidateID := "id"
	candidate := CandidateResponse{Id: candidateID}
	mockCandidateService := setupTest(t)
	request := &CandidateCreateRequest{Email: "user@email.com", FirstName: "John", LastName: "Doe"}
	mockCandidateService.
		EXPECT().
		Create(request).
		Return(&candidate).
		Times(1)
	defer mockCandidateService.ctrl.Finish()

	e := echo.New()
	reqJSON, _ := json.Marshal(request)
	req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(string(reqJSON)))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetHandler(Create)
	h := c.Handler()

	if assert.NoError(t, h(c)) {
		assert.Equal(t, http.StatusCreated, rec.Code)
		body, _ := ioutil.ReadAll(rec.Result().Body)
		var m CandidateResponse
		_ = json.Unmarshal(body, &m)
		assert.Equal(t, candidateID, m.Id)
	}
}

func TestCreateCandidateHandler_InvalidData_ThrowError(t *testing.T) {

	setupTest(t)
	mockCandidateService := setupTest(t)
	mockCandidateService.
		EXPECT().
		Create(gomock.Any()).
		Times(0)
	defer mockCandidateService.ctrl.Finish()

	e := echo.New()
	reqJSON := "{Email1: \"user@email.com\", FirstName1: \"John\", LastName1: \"Doe\"}"
	req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(string(reqJSON)))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetHandler(Create)
	h := c.Handler()

	if assert.NoError(t, h(c)) {
		assert.Equal(t, http.StatusBadRequest, rec.Code)
	}
}

func TestUpdateCandidateHandler(t *testing.T) {

	setupTest(t)
	candidateID := "id"
	candidate := CandidateResponse{Id: candidateID}
	mockCandidateService := setupTest(t)
	request := &CandidateUpdateRequest{Email: "user@email.com", FirstName: "John", LastName: "Doe"}
	mockCandidateService.
		EXPECT().
		Update(candidateID, request).
		Return(&candidate).
		Times(1)
	defer mockCandidateService.ctrl.Finish()

	e := echo.New()
	reqJSON, _ := json.Marshal(request)
	req := httptest.NewRequest(http.MethodPut, "/", strings.NewReader(string(reqJSON)))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetParamNames("id")
	c.SetParamValues(candidateID)
	c.SetPath("/candidates/:id")
	c.SetHandler(Update)
	h := c.Handler()

	if assert.NoError(t, h(c)) {
		assert.Equal(t, http.StatusOK, rec.Code)
		body, _ := ioutil.ReadAll(rec.Result().Body)
		var m CandidateResponse
		_ = json.Unmarshal(body, &m)
		assert.Equal(t, candidateID, m.Id)
	}
}

func TestUpdateCandidateHandler_InvalidData_BadRequest(t *testing.T) {

	setupTest(t)
	mockCandidateService := setupTest(t)
	mockCandidateService.
		EXPECT().
		Update(gomock.Any(), gomock.Any()).
		Times(0)
	defer mockCandidateService.ctrl.Finish()

	e := echo.New()
	reqJSON := "{Email1: \"user@email.com\", FirstName1: \"John\", LastName1: \"Doe\"}"
	req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(string(reqJSON)))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetHandler(Update)
	h := c.Handler()

	if assert.NoError(t, h(c)) {
		assert.Equal(t, http.StatusBadRequest, rec.Code)
	}
}
func TestDeleteHandler(t *testing.T) {

	setupTest(t)
	candidateID := "id"
	mockCandidateService := setupTest(t)
	mockCandidateService.
		EXPECT().
		Delete(candidateID).
		Times(1)
	defer mockCandidateService.ctrl.Finish()

	e := echo.New()
	RegisterCandidateRoutes(e)
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetParamNames("id")
	c.SetParamValues(candidateID)
	c.SetPath("/candidates/:id")
	c.SetHandler(Delete)
	h := c.Handler()

	if assert.NoError(t, h(c)) {
		assert.Equal(t, http.StatusNoContent, rec.Code)

	}
}
