package candidates

import (
	. "candidateApp2/candidates"
	"candidateApp2/tests/common"
	"context"
	"fmt"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

var mongodbFactory *common.MongoDbFactoryTestImpl = common.NewMongoDbFactoryTestImpl()

type CandidateCrudTest struct {
	CollectionNameSuffix string
	factory              *common.MongoDbFactoryTestImpl
	service              *CandidateServiceImpl
}

func (suite *CandidateCrudTest) SetupTest() {
	suite.CollectionNameSuffix = strconv.FormatInt(time.Now().UnixNano(), 10)
	mongodbFactory.CandidatesCollectionName = "candidates" + suite.CollectionNameSuffix
	suite.service = &CandidateServiceImpl{
		CandidateRepository: &CandidateRepositoryImpl{Factory: mongodbFactory}}
	suite.factory = mongodbFactory
}

func (suite *CandidateCrudTest) TearDownTest() {
	collection := suite.factory.GetCollection("candidates" + suite.CollectionNameSuffix)
	_ = collection.Drop(context.TODO())
}

func TestGetCandidates(t *testing.T) {

	suite := new(CandidateCrudTest)
	suite.SetupTest()
	defer suite.TearDownTest()
	collection := suite.factory.GetCollection("candidates" + suite.CollectionNameSuffix)
	first := CandidateModel{
		Id:        primitive.NewObjectID(),
		Email:     "john.doe@gmail.com",
		FirstName: "John",
		LastName:  "Doe"}
	second := CandidateModel{
		Id:        primitive.NewObjectID(),
		Email:     "jane.doe@gmail.com",
		FirstName: "Jane",
		LastName:  "Doe"}
	candidates := []interface{}{first, second}
	_, _ = collection.InsertMany(context.TODO(), candidates)

	r := suite.service.GetCandidates()

	assert.Equal(t, 2, len(r.Candidates))
	assert.Greater(t, len(r.Candidates[0].Id), 0)
	assert.Equal(t, r.Candidates[0].Email, first.Email)
	assert.Equal(t, r.Candidates[0].FirstName, first.FirstName)
	assert.Equal(t, r.Candidates[0].LastName, first.LastName)

}

func TestGetCandidateById(t *testing.T) {
	suite := new(CandidateCrudTest)
	suite.SetupTest()
	defer suite.TearDownTest()

	collection := suite.factory.GetCollection("candidates" + suite.CollectionNameSuffix)
	first := CandidateModel{
		Id:        primitive.NewObjectID(),
		Email:     "john.doe@gmail.com",
		FirstName: "John",
		LastName:  "Doe"}
	second := CandidateModel{
		Id:        primitive.NewObjectID(),
		Email:     "jane.doe@gmail.com",
		FirstName: "Jane",
		LastName:  "Doe"}
	candidates := []interface{}{first, second}
	ids, _ := collection.InsertMany(context.TODO(), candidates)
	id := ids.InsertedIDs[1].(primitive.ObjectID)

	r, _ := suite.service.GetCandidateById(id.Hex())

	assert.Equal(t, r.Email, second.Email)
	assert.Equal(t, r.FirstName, second.FirstName)
	assert.Equal(t, r.LastName, second.LastName)
}

func TestGetCandidateById_NotFound(t *testing.T) {

	suite := new(CandidateCrudTest)
	suite.SetupTest()
	defer suite.TearDownTest()

	collection := suite.factory.GetCollection("candidates" + suite.CollectionNameSuffix)
	first := CandidateModel{
		Id:        primitive.NewObjectID(),
		Email:     "john.doe@gmail.com",
		FirstName: "John",
		LastName:  "Doe"}
	second := CandidateModel{
		Id:        primitive.NewObjectID(),
		Email:     "jane.doe@gmail.com",
		FirstName: "Jane",
		LastName:  "Doe"}
	candidates := []interface{}{first, second}
	_, _ = collection.InsertMany(context.TODO(), candidates)

	notExistingId := primitive.NewObjectID()
	_, err := suite.service.GetCandidateById(notExistingId.Hex())

	expectedErrorString := fmt.Sprintf("Candidate with id '%s' not found", notExistingId.Hex())
	//noinspection GoNilness
	assert.EqualErrorf(t, err, expectedErrorString, err.Error())
	suite.TearDownTest()
}

func TestCreate(t *testing.T) {

	suite := new(CandidateCrudTest)
	suite.SetupTest()
	defer suite.TearDownTest()

	collection := suite.factory.GetCollection("candidates" + suite.CollectionNameSuffix)

	r := &CandidateCreateRequest{
		Email:     "john.doe@gmail.com",
		FirstName: "John",
		LastName:  "Doe"}

	result := suite.service.Create(r)
	var dbRecord CandidateModel
	objectId, _ := primitive.ObjectIDFromHex(result.Id)
	_ = collection.
		FindOne(context.TODO(), bson.D{{Key: "_id", Value: objectId}}).
		Decode(&dbRecord)

	assert.Equal(t, r.Email, dbRecord.Email)
	assert.Equal(t, r.FirstName, dbRecord.FirstName)
	assert.Equal(t, r.LastName, dbRecord.LastName)
}

func TestUpdate(t *testing.T) {

	suite := new(CandidateCrudTest)
	suite.SetupTest()
	defer suite.TearDownTest()

	collection := suite.factory.GetCollection("candidates" + suite.CollectionNameSuffix)

	r := &CandidateCreateRequest{
		Email:     "john.doe@gmail.com",
		FirstName: "John",
		LastName:  "Doe"}

	created := suite.service.Create(r)

	created.FirstName += "1"
	modelToUpdate := &CandidateUpdateRequest{
		Email:     "jane.wood@email.com",
		FirstName: "Jane",
		LastName:  "Wood",
	}
	updated := suite.service.Update(created.Id, modelToUpdate)
	var dbRecord CandidateModel
	objectId, _ := primitive.ObjectIDFromHex(created.Id)
	_ = collection.
		FindOne(context.TODO(), bson.D{{Key: "_id", Value: objectId}}).
		Decode(&dbRecord)

	assert.Equal(t, modelToUpdate.FirstName, dbRecord.FirstName)
	assert.Equal(t, modelToUpdate.FirstName, updated.FirstName)
	assert.Equal(t, modelToUpdate.LastName, dbRecord.LastName)
	assert.Equal(t, modelToUpdate.LastName, updated.LastName)
	assert.Equal(t, modelToUpdate.Email, dbRecord.Email)
	assert.Equal(t, modelToUpdate.Email, updated.Email)
}

func TestDelete(t *testing.T) {

	suite := new(CandidateCrudTest)
	suite.SetupTest()
	defer suite.TearDownTest()

	collection := suite.factory.GetCollection("candidates" + suite.CollectionNameSuffix)
	first := CandidateModel{
		Id:        primitive.NewObjectID(),
		Email:     "john.doe@gmail.com",
		FirstName: "John",
		LastName:  "Doe"}
	second := CandidateModel{
		Id:        primitive.NewObjectID(),
		Email:     "jane.doe@gmail.com",
		FirstName: "Jane",
		LastName:  "Doe"}
	candidates := []interface{}{first, second}
	_, _ = collection.InsertMany(context.TODO(), candidates)

	suite.service.Delete(second.Id.Hex())

	cur, _ := collection.
		Find(context.TODO(), bson.D{})
	defer cur.Close(context.TODO())

	var resultList []CandidateModel
	for cur.Next(context.TODO()) {
		var result CandidateModel
		_ = cur.Decode(&result)
		resultList = append(resultList, result)
	}

	assert.Equal(t, 1, len(resultList))
	assert.Equal(t, first.Id, resultList[0].Id)

}
