package common

import (
	"context"
	"fmt"
	"time"

	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoDbFactoryTestImpl struct {
	CandidatesCollectionName string
	ConnectionString         string
	Container                *testcontainers.Container
}

func NewMongoDbFactoryTestImpl() *MongoDbFactoryTestImpl {
	return &MongoDbFactoryTestImpl{}
}

func (m *MongoDbFactoryTestImpl) GetClient() (*mongo.Client, error) {
	if m.Container == nil {
		container, connectionString := createContainer()
		m.Container = container
		m.ConnectionString = connectionString
	}
	client, err := mongo.NewClient(options.Client().ApplyURI(m.ConnectionString))
	if err != nil {
		return nil, err
	}
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	err = client.Connect(ctx)
	if err != nil {
		return nil, err
	}
	return client, err
}

func (m *MongoDbFactoryTestImpl) GetCollection(name string) *mongo.Collection {
	client, _ := m.GetClient()
	return client.Database("devdb").Collection(name)
}
func (m *MongoDbFactoryTestImpl) GetCandidateCollection() *mongo.Collection {
	client, _ := m.GetClient()
	return client.Database("devdb").Collection(m.CandidatesCollectionName)
}

func createContainer() (*testcontainers.Container, string) {

	env := map[string]string{
		"MONGO_INITDB_ROOT_USERNAME": "owner",
		"MONGO_INITDB_ROOT_PASSWORD": "owner123",
	}
	req := testcontainers.ContainerRequest{
		Image:        "mongo:6",
		ExposedPorts: []string{"27017/tcp"},
		WaitingFor: wait.ForAll(
			wait.ForLog("Waiting for connections"),
			wait.ForListeningPort("27017/tcp"),
		),
		Env: env,
	}
	container, _ := testcontainers.GenericContainer(context.Background(), testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	mappedPort, _ := container.MappedPort(context.Background(), "27017")
	connectionString := fmt.Sprintf("mongodb://%s:%s@localhost:%s/?authSource=admin", env["MONGO_INITDB_ROOT_USERNAME"], env["MONGO_INITDB_ROOT_PASSWORD"], mappedPort.Port())
	return &container, connectionString
}
