package candidates

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

var CandidateServiceField CandidateService

func RegisterCandidateRoutes(e *echo.Echo) {
	e.GET("/candidates", GetAll)
	e.GET("/candidates/:id", GetById)
	e.POST("/candidates", Create)
	e.PUT("/candidates/:id", Update)
	e.DELETE("/candidates/:id", Delete)
}

func GetAll(c echo.Context) error {
	candidates := CandidateServiceField.GetCandidates()
	return c.JSON(http.StatusOK, candidates)
}

func GetById(c echo.Context) error {
	id := c.Param("id")
	candidate, err := CandidateServiceField.GetCandidateById(id)
	if err != nil {

		if nerr, ok := err.(*CandidateNotFoundError); ok {
			return echo.NewHTTPError(http.StatusNotFound, nerr.Error(), err)
		}
	}
	return c.JSON(http.StatusOK, candidate)
}

func Create(c echo.Context) error {
	req := new(CandidateCreateRequest)
	if err := c.Bind(req); err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	candidate := CandidateServiceField.Create(req)
	return c.JSON(http.StatusCreated, candidate)

}

func Update(c echo.Context) error {
	req := new(CandidateUpdateRequest)
	id := c.Param("id")
	if err := c.Bind(req); err != nil {
		return c.NoContent(http.StatusBadRequest)
	}
	candidate := CandidateServiceField.Update(id, req)

	return c.JSON(http.StatusOK, candidate)

}

func Delete(c echo.Context) error {
	id := c.Param("id")
	CandidateServiceField.Delete(id)

	return c.NoContent(http.StatusNoContent)

}
