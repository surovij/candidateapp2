package candidates

import (
	"candidateApp2/common"
	"context"

	"github.com/labstack/gommon/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//var Factory common.MongoDbFactory = common.NewMongoDbFactoryImpl()

func NewCandidateRepository(mongodbFactory common.MongoDbFactory) CandidateRepository {
	return &CandidateRepositoryImpl{Factory: mongodbFactory}
}

type CandidateRepository interface {
	getCandidates() <-chan []CandidateModel
	getCandidateById(id string) (<-chan CandidateModel, <-chan error)
	create(modelToInsert CandidateModel) (<-chan CandidateModel, <-chan error)
	update(modelToUpdate CandidateModel) (<-chan CandidateModel, <-chan error)
	delete(id string) <-chan error
}

type CandidateRepositoryImpl struct {
	Factory common.MongoDbFactory
}

func (c *CandidateRepositoryImpl) getCandidates() <-chan []CandidateModel {
	out := make(chan []CandidateModel, 1)
	go func() {
		collection := c.Factory.GetCandidateCollection()

		cur, _ := collection.Find(context.TODO(), bson.D{})
		defer cur.Close(context.TODO())

		var resultList []CandidateModel
		for cur.Next(context.TODO()) {
			var result CandidateModel
			_ = cur.Decode(&result)
			resultList = append(resultList, result)
		}
		out <- resultList
		close(out)
	}()
	return out
}

func (c *CandidateRepositoryImpl) getCandidateById(id string) (<-chan CandidateModel, <-chan error) {

	out := make(chan CandidateModel, 1)
	errorCh := make(chan error, 1)

	go func() {
		collection := c.Factory.GetCandidateCollection()

		var result CandidateModel
		objectID, _ := primitive.ObjectIDFromHex(id)
		err := collection.
			FindOne(context.TODO(), bson.D{{Key: "_id", Value: objectID}}).
			Decode(&result)

		if err != nil {
			log.Error(err)
			errorCh <- err
		}
		out <- result
		close(out)
		close(errorCh)
	}()
	return out, errorCh
}

func (c *CandidateRepositoryImpl) create(modelToInsert CandidateModel) (<-chan CandidateModel, <-chan error) {
	out := make(chan CandidateModel, 1)
	errorCh := make(chan error, 1)

	go func() {
		collection := c.Factory.GetCandidateCollection()

		r, err := collection.InsertOne(context.TODO(), modelToInsert)
		id := r.InsertedID.(primitive.ObjectID)

		if err != nil {
			log.Error(err)
			errorCh <- err
		}
		modelToInsert.Id = id
		out <- modelToInsert
		close(out)
		close(errorCh)
	}()
	return out, errorCh
}

func (c *CandidateRepositoryImpl) update(modelToUpdate CandidateModel) (<-chan CandidateModel, <-chan error) {
	out := make(chan CandidateModel, 1)
	errorCh := make(chan error, 1)

	go func() {
		collection := c.Factory.GetCandidateCollection()

		filter := bson.D{{Key: "_id", Value: modelToUpdate.Id}}
		_, err := collection.ReplaceOne(context.TODO(), filter, modelToUpdate)

		if err != nil {
			log.Error(err)
			errorCh <- err
		}

		out <- modelToUpdate
		close(out)
		close(errorCh)
	}()
	return out, errorCh
}

func (c *CandidateRepositoryImpl) delete(id string) <-chan error {

	errorCh := make(chan error, 1)

	go func() {
		collection := c.Factory.GetCandidateCollection()
		objectID, _ := primitive.ObjectIDFromHex(id)
		filter := bson.D{{Key: "_id", Value: objectID}}
		_, err := collection.DeleteOne(context.TODO(), filter)
		if err != nil {
			log.Error(err)
			errorCh <- err
		}
		close(errorCh)
	}()
	return errorCh
}
