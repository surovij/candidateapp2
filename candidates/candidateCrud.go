package candidates

import (
	"fmt"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type CandidateListResponse struct {
	Candidates []CandidateResponse `json:"candidates"`
}

type CandidateResponse struct {
	Id        string `json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"`
}

type CandidateCreateRequest struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"`
}

type CandidateUpdateRequest struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"`
}

type CandidateModel struct {
	Id        primitive.ObjectID `json:"_id" bson:"_id"`
	FirstName string
	LastName  string
	Email     string
}

type CandidateNotFoundError struct {
	Id string
}

func (m *CandidateNotFoundError) Error() string {
	return fmt.Sprintf("Candidate with id '%s' not found", m.Id)
}

type CandidateService interface {
	GetCandidates() CandidateListResponse
	GetCandidateById(id string) (*CandidateResponse, error)
	Create(r *CandidateCreateRequest) *CandidateResponse
	Update(id string, r *CandidateUpdateRequest) *CandidateResponse
	Delete(id string)
}

type CandidateServiceImpl struct {
	CandidateRepository CandidateRepository
}

func NewCandidateService(cr CandidateRepository) CandidateService {
	return &CandidateServiceImpl{CandidateRepository: cr}
}

func (s *CandidateServiceImpl) GetCandidates() CandidateListResponse {
	result := CandidateListResponse{Candidates: make([]CandidateResponse, 0)}
	m := <-s.CandidateRepository.getCandidates()
	for _, m := range m {
		cr := CandidateResponse{
			m.Id.Hex(),
			m.FirstName,
			m.LastName,
			m.Email,
		}
		result.Candidates = append(result.Candidates, cr)
	}
	return result
}

func (s *CandidateServiceImpl) GetCandidateById(id string) (*CandidateResponse, error) {

	chanModel, chanErr := s.CandidateRepository.getCandidateById(id)
	err := <-chanErr
	if err != nil && err.Error() == "mongo: no documents in result" {
		return nil, &CandidateNotFoundError{Id: id}
	}
	m := <-chanModel

	result := CandidateResponse{
		m.Id.Hex(),
		m.FirstName,
		m.LastName,
		m.Email,
	}

	return &result, nil
}

func (s *CandidateServiceImpl) Create(r *CandidateCreateRequest) *CandidateResponse {

	model := CandidateModel{
		Id:        primitive.NewObjectID(),
		FirstName: r.FirstName,
		LastName:  r.LastName,
		Email:     r.Email,
	}
	chanModel, _ := s.CandidateRepository.create(model)
	m := <-chanModel

	result := CandidateResponse{
		m.Id.Hex(),
		m.FirstName,
		m.LastName,
		m.Email,
	}

	return &result
}

func (s *CandidateServiceImpl) Update(id string, r *CandidateUpdateRequest) *CandidateResponse {

	objectId, _ := primitive.ObjectIDFromHex(id)
	model := CandidateModel{
		Id:        objectId,
		FirstName: r.FirstName,
		LastName:  r.LastName,
		Email:     r.Email,
	}
	chanModel, _ := s.CandidateRepository.update(model)
	m := <-chanModel

	result := CandidateResponse{
		m.Id.Hex(),
		m.FirstName,
		m.LastName,
		m.Email,
	}

	return &result
}

func (s *CandidateServiceImpl) Delete(id string) {

	<-s.CandidateRepository.delete(id)

}
