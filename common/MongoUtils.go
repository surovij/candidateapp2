package common

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoDbFactory interface {
	GetClient() (*mongo.Client, error)
	GetCollection(name string) *mongo.Collection
	GetCandidateCollection() *mongo.Collection
}

type MongoDbFactoryImpl struct {
	CandidatesCollectionName string
}

func NewMongoDbFactoryImpl() MongoDbFactory {
	return &MongoDbFactoryImpl{CandidatesCollectionName: "candidates"}
}

func (MongoDbFactoryImpl) GetClient() (*mongo.Client, error) {
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://owner:owner123@localhost:27017/?authSource=admin"))
	if err != nil {
		return nil, err
	}
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	err = client.Connect(ctx)
	if err != nil {
		return nil, err
	}
	return client, err
}

func (m MongoDbFactoryImpl) GetCollection(name string) *mongo.Collection {
	client, _ := m.GetClient()
	return client.Database("devdb").Collection(name)
}

func (m MongoDbFactoryImpl) GetCandidateCollection() *mongo.Collection {
	client, _ := m.GetClient()
	return client.Database("devdb").Collection(m.CandidatesCollectionName)
}
